import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import { App } from './App';

/*
function Componente(props){
  console.log(props);

  return (
    <div>
      <h1>{props.titulo}</h1>
      <div>{props.children}</div>
    </div>
  );
}*/

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <App />
);
