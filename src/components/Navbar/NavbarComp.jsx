import React, { Component } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link
  } from "react-router-dom";
import { HomePage } from '../../pages/HomePage';
import { GetPermissionsPage, } from '../../pages/GetPermissionsPage';
import { RequestPermissionPage } from '../../pages/RequestPermissionPage';
import { ModifyPermissionPage } from '../../pages/ModifyPermissionPage';
import { TypePermissionsPage } from '../../pages/TypePermissionsPage';

export default class NavbarComp extends Component{
    render(){
        return (
            <Router>
                <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand as={Link} to={"/"}>Challenge N5</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto">
                                <Nav.Link as={Link} to={"/permission"}>Permisos</Nav.Link>
                                <Nav.Link as={Link} to={"/typepermission"}>Tipo de Permisos</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
                </div>
                <div>
                <main>
                <Routes>
                    <Route path="/" element={<HomePage/>}></Route>
                    <Route exact path="/permission" element={<GetPermissionsPage/>}></Route>
                    <Route exact path="/permission/request" element={<RequestPermissionPage/>}></Route>
                    <Route exact path="/permission/modify/:id" element={<ModifyPermissionPage/>}></Route>
                    <Route exact path="/typepermission" element={<TypePermissionsPage/>}></Route>
                </Routes>
                </main>
                </div>
            </Router>
        );
    }
}