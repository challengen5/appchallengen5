import React, { useEffect, useState } from "react";
// import { Button } from "react-bootstrap";
// import { Link } from 'react-router-dom';
import DataTable from "react-data-table-component";
// import {FaPencilAlt} from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css'

import styles from './TypePermissions.module.css';
import { Spinner } from "../Spinner/Spinner";

import APIService from "../../services/APIService";

const columns = [
    {
        name: "ID", 
        selector: row => row.id, 
        sortable: true,
        omit: true
    },
    {
        name: "Acción",
        button: true,
        cell: row => row.id
		// cell: row => <Link to={"/typepermission/edit/" + row.id}><Button variant="warning" size="sm"><FaPencilAlt color="white"/></Button></Link>,
    },
    {
        name: "Descripcion", 
        selector: row => row.descripcion, 
        sortable: true, 
        grow: 3 
    }
];

const paginationOptions = {
    rowsPerPageText: "Filas por Página",
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: "Todos"
};

const headerTable = <div className="row">
    <div className="col-sm-8 col-md-10">Lista Tipo de Permisos</div>
    {/* <div className="col-sm-4 col-md-2"><Link to="/typepermission/add" className=""><Button variant="primary" >Nuevo</Button></Link></div> */}
</div>;

export function GetTypePermissions() {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        setIsLoading(true);

        APIService.get("/tipopermisos").then(res => {
            setData(res.data);
            setIsLoading(false);
        }).catch(console.log);
    }, []);

    if(isLoading){
        return <Spinner/>;
    }

    return (
        <div className={styles.permissionsTable}>
            <DataTable 
                columns={columns}
                data={data}
                title={headerTable}
                pagination
                paginationComponentOptions={paginationOptions}
                fixedHeader
                fixedHeaderScrollHeight="600px"
            />
        </div>
    )
}
