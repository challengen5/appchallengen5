import axios from "axios";
import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

import styles from './Permissions.module.css';
import { Spinner } from "../Spinner/Spinner";

import APIService from "../../services/APIService";

export function RequestPermission() {
    const navigate = useNavigate();

    const [isLoading, setIsLoading] = useState(true);
    const [isPending, setIsPending] = useState(false);

    const [tipoPermisos, setTipoPermisos] = useState([]);

    const [data, setData] = useState({
        Id: 0,
        NombreEmpleado: '',
        ApellidoEmpleado: '',
        TipoPermiso: 0,
        FechaPermiso: ''
    });

    useEffect(() => {
        setIsLoading(true);

        function fetchTipoPermisos(){
            APIService.get('/tipopermisos').then(res => {
                setTipoPermisos(res.data);
                setIsLoading(false);
            }).catch(console.log);
        }

        fetchTipoPermisos();
    }, []);

    if(isLoading){
        return <Spinner/>;
    }

    const handleInputChange = (e) => {
        setData({
            ...data,
            [e.target.name] : e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(data.NombreEmpleado === '' || data.ApellidoEmpleado === '' || data.TipoPermiso === 0 || data.FechaPermiso === '') {
            alert('¡Hay campos vacíos!');
            return;
        }

        setIsPending(true);

        const url = `${process.env.REACT_APP_CN5API_BASEURL}/permisos`;

        axios({
            method: 'post',
            url: url,
            data: {
                Id: 0, 
                NombreEmpleado: data.NombreEmpleado, 
                ApellidoEmpleado: data.ApellidoEmpleado,
                TipoPermiso: parseInt(data.TipoPermiso),
                FechaPermiso: data.FechaPermiso
            }
        })
        .then(res => {
            setIsPending(false);
            navigate("/permission")
        }).catch(() => {
            setIsPending(false);
        });
    };

    return (
        <div className={styles.permissions}>
            <Card>
                <Card.Header><h2>Solicitar Permiso</h2></Card.Header>
                <Card.Body>
                    <div>
                        <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3">
                                <Form.Label>Nombre Empleado</Form.Label>
                                <Form.Control 
                                    type="text"
                                    name="NombreEmpleado"
                                    value={data.NombreEmpleado} 
                                    onChange={handleInputChange} 
                                    placeholder="Nombre del Empleado"/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Apellido Empleado</Form.Label>
                                <Form.Control 
                                    type="text"
                                    name="ApellidoEmpleado"
                                    value={data.ApellidoEmpleado} 
                                    onChange={handleInputChange} 
                                    placeholder="Apellido del Empleado"/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Tipo Permiso</Form.Label>
                                <Form.Select
                                    name="TipoPermiso"
                                    value={data.TipoPermiso} 
                                    onChange={handleInputChange}
                                >
                                    <option value="0" disabled>-- Seleccionar --</option>
                                    {tipoPermisos.map(item => <option key={item.id} value={item.id}>{item.descripcion}</option> )}
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Fecha Permiso</Form.Label>
                                <Form.Control 
                                    type="date"
                                    name="FechaPermiso"
                                    value={data.FechaPermiso} 
                                    onChange={handleInputChange} 
                                    placeholder="Fecha del Permiso"/>
                            </Form.Group>
                            
                            { !isPending && <Button variant="primary" type="submit">Solicitar</Button> }
                            { isPending && <Button variant="primary" disabled type="submit">Solicitando...</Button> }
                        </Form>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}
