import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import DataTable from "react-data-table-component";
import {FaPencilAlt} from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css'

import styles from './Permissions.module.css';
import { Spinner } from "../Spinner/Spinner";

import APIService from "../../services/APIService";

const paginationOptions = {
    rowsPerPageText: "Filas por Página",
    rangeSeparatorText: 'de',
    selectAllRowsItem: true,
    selectAllRowsItemText: "Todos"
};

const headerTable = <div className="row">
    <div className="col-sm-8 col-md-10">Lista de Permisos</div>
    <div className="col-sm-4 col-md-2"><Link to="/permission/request" className=""><Button variant="primary" >Solicitar Permiso</Button></Link></div>
</div>;

export function GetPermissions() {
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const [tipoPermisos, setTipoPermisos] = useState([]);

    function getTipoPermiso(id){
        const tp = tipoPermisos.find(item => item.id === id);
        return tp.descripcion;
    }

    useEffect(() => {
        setIsLoading(true);

        function fetchData(){
            APIService.get('/permisos').then(res => {
                setData(res.data);
                setIsLoading(false);
            }).catch(console.log);
        }

        function fetchTipoPermisos(){
            APIService.get('/tipopermisos').then(res => {
                setTipoPermisos(res.data);
                fetchData();
            }).catch(console.log);
        }
        
        fetchTipoPermisos();

    }, []);

    if(isLoading){
        return <Spinner/>;
    }

    const columns = [
        {
            name: "ID", 
            selector: row => row.id, 
            sortable: true,
            omit: true
        },
        {
            name: "Acción",
            button: true,
            cell: row => <Link to={"/permission/modify/" + row.id}><Button variant="warning" size="sm"><FaPencilAlt color="white"/></Button></Link>,
        },
        {
            name: "Nombre Empleado", 
            selector: row => row.nombreEmpleado, 
            sortable: true, 
            grow: 3 
        },
        {
            name: "Apellido Empleado", 
            selector: row => row.apellidoEmpleado, 
            sortable: true, 
            grow: 3 
        },
        {
            name: "Tipo Permiso",
            selector: row => getTipoPermiso(row.tipoPermiso), 
            sortable: true, 
            grow: 2 
        },
        {
            name: "Fecha Permiso", 
            selector: row => row.strFechaPermiso, 
            sortable: true
        },
    ];

    return (
        <div className={styles.permissionsTable}>
            <DataTable 
                columns={columns}
                data={data}
                title={headerTable}
                pagination
                paginationComponentOptions={paginationOptions}
                fixedHeader
                fixedHeaderScrollHeight="600px"
            />
        </div>
    )
}
