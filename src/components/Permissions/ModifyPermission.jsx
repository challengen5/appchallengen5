import axios from "axios";
import { useEffect, useState } from "react";
import { Card, Form, Button } from "react-bootstrap";
import { useParams, useNavigate } from "react-router-dom";

import { Spinner } from "../Spinner/Spinner";
import styles from './Permissions.module.css';

import APIService from "../../services/APIService";

export function ModifyPermission() {
    const { id } = useParams();
    const navigate = useNavigate();

    const [isLoading, setIsLoading] = useState(true);
    const [isPending, setIsPending] = useState(false);

    const [tipoPermisos, setTipoPermisos] = useState([]);

    const [data, setData] = useState({
        Id: 0,
        NombreEmpleado: '',
        ApellidoEmpleado: '',
        TipoPermiso: 0,
        FechaPermiso: ''
    });

    useEffect(() => {
        setIsLoading(true);

        function fetchData(){
            APIService.get(`/permisos/${id}`).then(res => {
                const result = res.data;
                setData({
                        Id: result.id, 
                        NombreEmpleado: result.nombreEmpleado, 
                        ApellidoEmpleado: result.apellidoEmpleado,
                        TipoPermiso: result.tipoPermiso,
                        FechaPermiso: result.strFechaPermiso
                    });

                setIsLoading(false);
            }).catch(console.log);
        }
        
        function fetchTipoPermisos(){
            APIService.get('/tipopermisos').then(res => {
                setTipoPermisos(res.data);
                fetchData();
            }).catch(console.log);
        }
        
        fetchTipoPermisos();

    }, [id]);

    if(isLoading){
        return <Spinner/>;
    }

    const handleInputChange = (e) => {
        setData({
            ...data,
            [e.target.name] : e.target.value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if(data.NombreEmpleado === '' || data.ApellidoEmpleado === '' || data.TipoPermiso === 0 || data.FechaPermiso === '') {
            alert('¡Hay campos vacíos!');
            return;
        }
        
        setIsPending(true);

        const url = `${process.env.REACT_APP_CN5API_BASEURL}/permisos/${id}`;

        axios({
            method: 'put',
            url: url,
            data: {
                Id: data.Id, 
                NombreEmpleado: data.NombreEmpleado, 
                ApellidoEmpleado: data.ApellidoEmpleado,
                TipoPermiso: parseInt(data.TipoPermiso),
                FechaPermiso: data.FechaPermiso
            }
        })
        .then(res => {
            console.log(res);
            navigate("/permission")
            setIsPending(false);
        }).catch(() => {
            setIsPending(false);
        });
    };

    return (
        <div className={styles.permissions}>
            <Card>
                <Card.Header><h2>Modificar Permiso</h2></Card.Header>
                <Card.Body>
                    <div>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group className="mb-3">
                                <Form.Label>Nombre Empleado</Form.Label>
                                <Form.Control 
                                    type="text"
                                    name="NombreEmpleado"
                                    value={data.NombreEmpleado} 
                                    onChange={handleInputChange} 
                                    placeholder="Nombre del Empleado"/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Apellido Empleado</Form.Label>
                                <Form.Control 
                                    type="text"
                                    name="ApellidoEmpleado"
                                    value={data.ApellidoEmpleado} 
                                    onChange={handleInputChange} 
                                    placeholder="Apellido del Empleado"/>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Tipo Permiso</Form.Label>
                                <Form.Select
                                    name="TipoPermiso"
                                    value={data.TipoPermiso} 
                                    onChange={handleInputChange}
                                >
                                    <option value="0" disabled>-- Seleccionar --</option>
                                    {tipoPermisos.map(item => <option key={item.id} value={item.id}>{item.descripcion}</option> )}
                                </Form.Select>
                            </Form.Group>

                            <Form.Group className="mb-3">
                                <Form.Label>Fecha Permiso</Form.Label>
                                <Form.Control 
                                    type="date"
                                    name="FechaPermiso"
                                    value={data.FechaPermiso} 
                                    onChange={handleInputChange} 
                                    placeholder="Fecha del Permiso"/>
                            </Form.Group>
                            
                            { !isPending && <Button variant="success" type="submit">Modificar</Button> }
                            { isPending && <Button variant="success" disabled type="submit">Modificando...</Button> }
                        </Form>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}
